const Ajv = require("ajv")
const fs = require('fs')
const YAML = require("yaml")

const normalize = (data) => {
  data["abteilungen"].forEach((division) => {
    division["personen"] = division["personen"] || []
    division["dokumente"] = division["dokumente"] || []
    division["dokumente"].forEach((doc) => {
      doc["nur_anzeigen_wenn_unterkategorie_eine_von"] = doc["nur_anzeigen_wenn_unterkategorie_eine_von"] || []
    })
  })
}

const ajv = new Ajv() // options can be passed, e.g. {allErrors: true}

const schemaData = fs.readFileSync('./schemas/schema.json', 'utf8')
const schema = JSON.parse(schemaData)

const dataYAML = fs.readFileSync('../daten/daten.yml', 'utf8')
const parsedYAML = YAML.parse(dataYAML)

const validCategories = parsedYAML["kategorien"].map((category) => {
  return category["id"]
})

const validSubCategories = parsedYAML["unterkategorien"].map((subcategory) => {
  return subcategory["id"]
})

const validDivisions = parsedYAML["abteilungen"].map((division) => {
  return division["id"]
})

schema["$defs"]["unterkategorie_eintrag"]["properties"]["kategorie"]["enum"] = validCategories
schema["$defs"]["unterkategorie_eintrag"]["properties"]["ziel_abteilung"]["enum"] = validDivisions
schema["$defs"]["document"]["properties"]["nur_anzeigen_wenn_unterkategorie_eine_von"]["items"]["enum"] = validSubCategories
schema["$defs"]["person"]["properties"]["nur_verantwortlich_für"]["items"]["enum"] = validSubCategories

normalize(parsedYAML)

validate = ajv.compile(schema);


const valid = validate(parsedYAML)

if (!valid) {
  console.log(JSON.stringify(validate.errors, null, 2))

  process.exit(1)
} else {

  fs.writeFileSync("../frontend/src/data/data.json", JSON.stringify(parsedYAML), {
    encoding: "utf8",
    flag: "w"
  });
  console.log("Your data files are valid.")
}

